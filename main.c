#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <unistd.h>

#include "own_DNS.h"

#pragma comment(lib,"ws2_32.lib")

#define DNS_SERVER_PORT 53
#define DNS_SERVER "208.67.222.222"
#define MAX_BUFFER_SIZE 65536
#define DNS_HEADER_SIZE 12

#define QUERY_ALWAYS 0
#define OPCODE_VALUE 0 // standard query
#define AUTHORITATIVE 0 // authoritative not needed
#define TRUNCATED 0 // not truncated
#define RECURSION 1 // not desired 
#define RECURSION_AVAILABLE 0 // not available 
#define EVIL_BIT 0 // never used
#define AUTHENTICATED_DATA 0 // is the data authenticated by server
#define CHECKING_DISABLED 0
#define RESPONSE_CODE 0 // only for response
#define SINGLE_QUERY 1 //only 1 query
#define SINGLE_ANSWER 0 //only for response
#define AUTHORITATIVE_RESPO 0 //for response
#define TOTAL_ADDITIONAL_RRS 0 //for response
#define IP_v4 1

const char *byte_to_binary(int );
struct DNS_HEADER *dns=NULL;

struct CONSTANT_CONTENT_PRE_DATA // after DNS header there is some data preceded by constant size content included in that struct
{
    unsigned short type;
    unsigned short _class;
    unsigned int ttl;
    unsigned short data_len;
};

struct POINTERS_TO_RECV_DATA // after DNS header and constant size content there is data of variable lenth so we need pointers to read it
{
    unsigned char *name;
    struct CONSTANT_CONTENT_PRE_DATA *resource;
    unsigned char *rdata;
};

int main(int argc, char *argv[]) 
{
	SOCKET s;	
	struct sockaddr_in server, answers_addr;
	const int query_type = 1; //based on RFC 1035  
	unsigned short int query_type_IPv4=1, query_class=1;
	int offset=0, address_size=0,i, j, p, counter, strnlen_tmp, dot_counter=0;	
	char *start;
	struct POINTERS_TO_RECV_DATA dns_answers[20];
	char host[255];
	memset(host,'\0', 256);

	host[0]='.';
	if(argc == 2){
		memcpy(host+1, argv[1], strlen(argv[1]));
	} 
	else{
		printf("program accepts one parameter");
		return 0;
	}
	
	strnlen_tmp=strlen(host);
	host[strnlen_tmp]='.';
	host[strnlen_tmp+1]='\0';
	strnlen_tmp=strlen(host);
	j=0;
	counter=0;
	
	for(i=0;i<strnlen_tmp;i++)
	{
		counter++;
		if(host[i]=='.')
		{
			dot_counter++;
			host[j]=counter-1;
			j=i;
			counter=0;		
		}
	}
	host[strnlen_tmp-1]=0;
	if(dot_counter < 3){
		printf("invalid host name");
		return 0;
	}

	char buffer[MAX_BUFFER_SIZE], buffer_for_reply[MAX_BUFFER_SIZE];

	WSADATA wsa;
	WSAStartup(MAKEWORD(2,2),&wsa);	
	query_type_IPv4=htons(1); query_class=htons(1);
	
	address_size = sizeof(server);
	
	dns = (struct DNS_HEADER *) malloc (sizeof(struct DNS_HEADER));
	dns->id = (unsigned short)htons(_getpid()); // identification number   htons getpid
	dns-> qr = 0; // query/response flag
    dns-> opcode = 0; // purpose of message  
    dns-> aa = 0; // authoritive answer
    dns-> tc = 0; // truncated message
	dns-> rd = 1; // recursion desired		
	
    dns-> ra = 0 ; // recursion available
    dns-> z = 0; // its z! reserved
    dns-> ad = 0; // authenticated data  
    dns-> cd = 0; // checking disabled
    dns-> rcode =0; // response code
	
    dns-> q_count = htons(1) ; // number of question entries
    dns-> ans_count = 0; // number of answer entries
    dns-> auth_count = 0; // number of authority entries
    dns-> add_count = 0; // number of resource entries
    
    
	offset=sizeof(struct DNS_HEADER);
	memcpy (buffer, dns, offset);
	memcpy ((buffer+offset), host, strlen(host)+1);
	offset+=strlen(host)+1;
	memcpy((buffer + offset), &query_type_IPv4, sizeof(query_type_IPv4));
	offset+=sizeof(short);
	memcpy((buffer + offset), &query_class, sizeof(query_class));
	offset+=sizeof(short);

    server.sin_family = AF_INET;
    server.sin_port = htons(53);
	server.sin_addr.s_addr = inet_addr("208.67.222.222");	

	if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==SOCKET_ERROR)
	printf("error error %d" , WSAGetLastError());

    if (sendto(s, buffer, offset , 0 , (struct sockaddr *) &server, address_size) == SOCKET_ERROR)
    printf("send error %d" , WSAGetLastError());
  	
  	memset(buffer,'\0', MAX_BUFFER_SIZE);
 	if (recvfrom(s,(char*)buffer, MAX_BUFFER_SIZE, 0, (struct sockaddr *) &server, &address_size) == SOCKET_ERROR)
	printf("recv error %d" , WSAGetLastError());
    
	
	start=&buffer[sizeof(struct DNS_HEADER) + (strlen((const char*)host)+1) + sizeof(query_type_IPv4)+sizeof(query_class)];
 	offset=start-buffer;
	dns = (struct DNS_HEADER*)&buffer;
	  	
	  offset=offset+12;
    
    for(i=0;i<(int)strlen((const char*)host);i++)
    {
        p=host[i];
        for(j=0;j<(int)p;j++)
        {
            host[i]=host[i+1];
            i=i+1;
        }
        host[i]='.';
    }
    host[i-1]='\0'; //remove the last dot

	for(i=0;i<ntohs(dns->ans_count);i++)
	{
		
		for(j=0;j<4;j++)
		{
			if(j==3)
			{
				printf("%d", (unsigned char)buffer[offset]);
				offset++;
			}
			else
			{
				printf("%d.", (unsigned char)buffer[offset]);
				offset++;
			}
		}
		offset=offset+12;
		printf("\n");
	}
	printf("\n");
	free(dns);
	closesocket(s);
	return 0;
}

const char *byte_to_binary(int x)
{
    static char b[9];
	b[0] = '\0';

    int z;
    for (z = 128; z > 0; z >>= 1)
    {
        strcat(b, ((x & z) == z) ? "1" : "0");
    }

    return b;
}




